const svgDelete = `<svg class="btn-delete" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M135.2 17.7C140.6 6.8 151.7 0 163.8 0H284.2c12.1 0 23.2 6.8 28.6 17.7L320 32h96c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 96 0 81.7 0 64S14.3 32 32 32h96l7.2-14.3zM32 128H416V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V128zm96 64c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16z"/></svg>`
const svgEdit = `<svg class="btn-edit" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z"/></svg>`
const svgTick = `<svg class="btn-tick" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M470.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L192 338.7 425.4 105.4c12.5-12.5 32.8-12.5 45.3 0z"/></svg>`
const svgCross = `<svg class="btn-cross" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M310.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L160 210.7 54.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L114.7 256 9.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L160 301.3 265.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L205.3 256 310.6 150.6z"/></svg>`
const createPost = `<div class="create-post"><form action="" method="POST" class="post-form card"><div class="about-post">New post<input type="text" name="title" id="title" placeholder="Title" required><textarea name="text" id="text" placeholder="Write something" ></textarea></div><button class="btn-create">Create a new post</button><button class="btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M310.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L160 210.7 54.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L114.7 256 9.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L160 301.3 265.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L205.3 256 310.6 150.6z"/></svg></button></form></div>`;

const listCards = document.querySelector('.list-cards');
const btnAddPost = document.querySelector('.btn-add');

const users = getData('https://ajax.test-danit.com/api/json/users');
const posts = getData('https://ajax.test-danit.com/api/json/posts');
const photos = getData('js/photo-users.json');

class Card {
    constructor(userName, userEmail, postTitle, postText, postId, userPhoto = '#') {
        this.userName = userName;
        this.userEmail = userEmail;
        this.postTitle = postTitle;
        this.postText = postText;
        this.postId = postId;
        this.userPhoto = userPhoto
    }

    createCardHtml() {
        this.card = document.createElement('div');
        this.card.id = `${this.postId}`;
        this.card.classList.add(`card`);
        this.card.insertAdjacentHTML('beforeend', `
        <div class="card-photo">
        <img src="${this.userPhoto}" alt="photo user"></div>
        <div class="card-main">
            <div class="about-user">
                <div class="full-name">${this.userName}</div>
                <a class="email">${this.userEmail}</a>
            </div>
            <div class="about-post">
                <div class="title">${this.postTitle}</div>
                <div class="text">${this.postText}</div>
                ${svgEdit}
                ${svgDelete}
        </div>
        </div>`)
        listCards.insertAdjacentElement('afterbegin', this.card);

        this.editCard();
        this.deleteCard();

    }

    deleteCard() {
        this.card.addEventListener('click', event => {
            const itemDelete = event.target.closest('.btn-delete');
            if (itemDelete) {
                if (this.postId <= 100) {
                    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, 
                    {
                        method: "DELETE",
                    })
                    .then(response => {
                        if(response.status === 200) this.card.remove()
                    })
                } else {
                    this.card.remove()
                }
            }
        });
    }

    editCard () {
        this.card.addEventListener('click', event => {
            const itemEdit = event.target.closest('.btn-edit');
            if (itemEdit) {
                const cardHtml = document.getElementById(`${this.postId}`);
                const cardMain = cardHtml.children[1];
                const aboutPost = cardMain.children[1];
                aboutPost.innerHTML = `<input class="title" id="title" type="text" value="${this.postTitle}">
                <textarea id="text" type="text">${this.postText}</textarea>
                ${svgTick}
                ${svgCross}`
                const tick = aboutPost.children[2];
                const cross = aboutPost.children[3];
                tick.addEventListener('click', this.updateCard())
                cross.addEventListener('click', ev => {
                aboutPost.innerHTML = `<div class="title">${this.postTitle}</div><div class="text">${this.postText}</div>${svgEdit}${svgDelete}`
                })
            }
        });
    }

    updateCard() {
        this.card.addEventListener('click', event => {
            const itemTick = event.target.closest('.btn-tick');
            if (itemTick) {
                const post = event.currentTarget.closest('.card');
                const cardMain = post.children[1];
                const aboutPost = cardMain.children[1];
                const title = aboutPost.children[0];
                const text = aboutPost.children[1];
                if (validData(title, text)) {
                    if (post.id <= 100) {
                        this.putCard(title.value, text.value).then(updatePost => {
                            aboutPost.innerHTML = `<div class="title">${updatePost.title}</div>
                            <div class="text">${updatePost.body}</div>
                            ${svgEdit}
                            ${svgDelete}`
                        })
                    } else {
                        aboutPost.innerHTML = `<div class="title">${title.value}</div><div class="text">${text.value}</div>${svgEdit}${svgDelete}`
                    }
                }
                this.postTitle = title.value;
                this.postText = text.value;
                
            }
        })
    }

    putCard(title, text) {
        return fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
            method: "PUT",
            body: JSON.stringify({
                userId: 1,
                title: title,
                body: text
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
    }
}

start()


function start() {
    btnAddPost.addEventListener('click', createNewPost);    
    Promise.all([users, posts, photos])
    .then(data => {
        const [users, posts, photos] = data;
        listCards.classList.remove('loader');
        posts.forEach(post => {
            findUser(post.userId, users, photos);
            createCard(`${name}`, `${email}`, `${post.title}`, `${post.body}`, `${post.id}`, `${photo}`)
        });

    })
    
}
function getData(url) {
    return fetch(url, {
    method: "GET"
    })
    .then(response => response.json())
}
function createCard (name, email, title, text, postId, photo) {
    const card = new Card(name, email, title, text, postId, photo);
    card.createCardHtml();
}
function findUser(userIdPost, arrUsers, arrPhoto) {
    arrUsers.forEach(elem => {
        if(elem.id === userIdPost) return { name, email } = elem;
    })
    arrPhoto.forEach(e => {
        if(e.userId === userIdPost) return { photo } = e;
    })
}
function createNewPost() {
    const body = document.querySelector('body')
    body.style.overflow = "hidden"
    btnAddPost.insertAdjacentHTML('afterend', createPost);
    const btnClose = document.querySelector('.btn-close');
    const btnCreate = document.querySelector('.btn-create');
    const createPostHtml = document.querySelector('.create-post');
    btnClose.addEventListener('click', ev => {
        createPostHtml.remove();
        body.style.overflow = "scroll";
    })
    btnCreate.addEventListener('click', addNewPostHtml)
}
function validData(title, text) {
    if (title.value.length === 0 || text.value.length === 0) {
        if(title.value.length === 0) {
            title.style.border = '1px solid red'
        } else {
            title.style.border = '1px solid green'
        };
        if(text.value.length === 0) {
            text.style.border = '1px solid red'
        } else {
            text.style.border = '1px solid green'
        };
    } else {
        return true;
    }
}
function addNewPostHtml(event) {
    event.preventDefault()
    const post = event.currentTarget.closest('.card');
    const aboutPost = post.children[0];
    const title = aboutPost.children[0];
    const text = aboutPost.children[1];
    if (validData(title, text)) {
        postPost(title.value, text.value)
        const body = document.querySelector('body')
        body.style.overflow = "scroll";
        const createForm = document.querySelector('.create-post')
        createForm.remove()
    }
}       
function postPost(title, text) {
    return Promise.all([users, posts, photos])
        .then(data => {
            const [users, posts, photos] = data;
            const newId = posts[posts.length - 1].id += 1;
            fetch('https://ajax.test-danit.com/api/json/posts', {
                method: 'POST',
                body: JSON.stringify({
                    userId: title,
                    body: text,
                    id: newId
                }),
                headers: {
                'Content-Type': 'application/json'
                }
            })
            .then(response => response.json())
            .then(post => createCard(`${users[0].name}`, `${users[0].email}`, `${title}`, `${text}`, `${newId}`, `${photos[0].photo}`))
        })
}