// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідування здійснюється за допомогою функцій. 
// У кожного об'єкта є посилання на інший об'єкт(__proto__), що є його прототипом (ptototype). Цей прототипний об'єкт має також посилання або на себе або на інший об'єкт, що є вже його прототипом і так далі, поки не буде досягнуто null (null не має прототипу).

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
//super() потрібен для виклику батківського конструктора;

class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name(){
        return this._name;
    }
    set name(valueName){
        return this._name = valueName;
    }
    get age(){
        return this._age;
    }
    set age(valueAge){
        return this._age = valueAge;
    }
    get salary(){
        return this._salary;
    }
    set salary(valueSalary){
        return this._salary = valueSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(...arguments);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
}

const petr = new Programmer('Petr', 32, 3000, 'english, ukr');
const anna = new Programmer('Anna', 22, 4000, 'english, ukr, german');
const ivan = new Programmer('Ivan', 19, 2500, 'ukr');

console.log(petr)
console.log(petr.salary)
console.log(anna)
console.log(ivan)
