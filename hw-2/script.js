// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// доречно використовувати тоді коли ми не хочемо щоб наш код впав а робив щось далі. Наприклад коли ми не знаємо який массив нам прийде з сервера (він може бути не повний, з пропусками значень як у нашому завдані). А також використовувати тоді коли ми можемо припуститися помилки и щоб це не стало фатально, з метою убезпечити себе можна використати try...catch

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

showArr(books);

function validationData(obj, indexObjInArray, ...propertes) {
    let count = 0;
    propertes.forEach(prop => {
        if(!obj.hasOwnProperty(prop)) {
            console.log(`Object with index in array: ${indexObjInArray} doesn't have ${prop}`);
        } else {
            count++;
        }
    })
    if(count === propertes.length) return true;
}

function createObj(obj) {
    const divRoot = document.querySelector('#root');
    const ul = document.createElement('ul');
    const li = document.createElement('li');
    let text = '';
    for(const key in obj) {
        text += `${[key]} - ${obj[key]}; `;
    }
    li.innerText = text;
    ul.append(li);
    divRoot.append(ul);
}

function showArr(arr) {
    arr.forEach(obj => {
        let currentIndex = arr.indexOf(obj)
        if(validationData(obj, currentIndex, 'author', 'name', 'price')) createObj(obj)
    })
}




