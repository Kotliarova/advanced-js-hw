// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
//Javascript - це однопоточна мова програмування, тобто код виконується в одному стеку викликів. Але в JS є також можливість виконувати асинхроний код. Для асинхронного виконання коду можна застосовувати: таймери, promise або async & await


const btnIp = document.querySelector('.btn-ip');
btnIp.addEventListener('click', getAdress);

async function getAdress() {
    const responseIp = await fetch('https://api.ipify.org/?format=json');
    const { ip } = await responseIp.json();
    const responseAdress = await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`);
    const adress = await responseAdress.json();
    const { status, continent, country, regionName, city, district } = adress;
    if(status === 'success') {
        createAdressHtml(continent, country, regionName, city, district)
    }
}

function createAdressHtml(continent, country, regionName, city, district) {
    btnIp.insertAdjacentHTML('afterend', `<div class="adress">
    <div class="continent">Continent: ${continent || 'unknown'}</div>
    <div class="country">Country: ${country || 'unknown'}</div>
    <div class="regionName">Region: ${regionName || 'unknown'}</div>
    <div class="city">City: ${city || 'unknown'}</div>
    <div class="district">District: ${district || 'unknown'}</div>
    </div>`)
}